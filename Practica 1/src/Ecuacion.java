import java.lang.Math;
public class Ecuacion {
	private double valorA;
	private double valorB;
	private double valorC;
	private double raiz1;
	private double raiz2;
	private int cantRaiz;
		
	public Ecuacion (double va,double vb,double vc){
		valorA=va;
		valorB=vb;
		valorC=vc;
		
		if (((Math.pow(valorB,2))-4*valorA*valorC) >0){
			raiz1 = (-valorB + Math.sqrt((Math.pow(valorB,2))-(4*valorA*valorC)))/(2*valorA);
		    raiz2 = (-valorB - Math.sqrt((Math.pow(valorB,2))-(4*valorA*valorC)))/(2*valorA);
		    cantRaiz = 2;
		}else if (((Math.pow(valorB,2))-4*valorA*valorC) == 0){
				raiz1 = (-valorB + Math.sqrt((Math.pow(valorB,2))-(4*valorA*valorC)))/(2*valorA);
				raiz2 = raiz1;
				cantRaiz = 1;
		}else {
			raiz1=0;
			raiz2=0;
			cantRaiz=0;
		}

	}
	
	public double [] calcularRaices() {
		double [] raices = {0,0,0};
		if (cantRaiz == 1) {
			raices [0] = 1;
			raices [1] = raiz1;
		} else if (cantRaiz == 2) {
			raices [0] = 2;
			raices [1] = raiz1;
			raices [2] = raiz2;
					
		}
		return raices;
		
	}
}
