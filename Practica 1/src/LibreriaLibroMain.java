import java.util.*;

public class LibreriaLibroMain {

	
		public static void main(String[] args) {
			Scanner leer = new Scanner(System.in);
			int librosPrestados=0;
			
			//Cargo los objetos en un array.
			Libro [] libros;
			libros = new Libro [3];
			libros[0] = new Libro(101,"Cuentos","Diego",100,10);
			libros[1] = new Libro(102,"Cuentos Cortos","Emanuel",300,11);
			libros[2] = new Libro(103,"Cuentos Largos","Jose",400,1);
			int i=0;
			
			//Muestro los libros cargados.
			System.out.println("--------------------------------------------------------");
			System.out.printf("%-5s %-15s %-10s %-10s %-5s", "ISBN", "TITULO", "AUTOR", "PAGINAS", "DISPONIBLES");
			System.out.println();
			System.out.println("--------------------------------------------------------");
			while (i < 3){
				System.out.format("%-5s %-15s %-10s %-10s %-5s",libros[i].GetISBN(), libros[i].GetTitulo(), libros[i].GetAutor(),libros[i].GetPaginas(),libros[i].GetEjemplaresDisponibles());
				System.out.println();
				i++;
			}
			System.out.println("--------------------------------------------------------");
			
			//Busco el libro con mayor cantidad de paginas y lo muestro en pantalla.
			int mayor =0;
			i=0;
			while (i<3) {
				if (libros[i].GetCantPaginas()>= mayor) {
					mayor=i;
				}
				i++;
			}
			System.out.println("El libro que mas paginas tiene es: " + libros[mayor].GetTitulo());
			
			
			System.out.println("La Cantidad de prestamos es : " + librosPrestados);
			
			//Pido el alquiler de un libro.
			System.out.println();
			System.out.println("POR FAVOR INGRESE EL NOMBRE DEL TITULO A PEDIR...");
			String vAutor="";
			vAutor=leer.nextLine();
			
			i=0;
			while (i<3) {
				if (vAutor.equalsIgnoreCase(libros[i].GetTitulo())) {
					if (libros[i].GetEjemplaresDisponibles()>1) {
					libros[i].SetEjemplaresPrestados(libros[i].GetEjemplaresPrestados()+1);
					libros[i].SetEjemplaresDisponibles(libros[i].GetEjemplaresDisponibles()-1);
					librosPrestados += 1;
					System.out.println("SU PRESTAMO FUE INGRESADO");
					}else {
						System.out.println("NO SE PUEDE PRESTAR EL LIBRO POR NO TENERLO DISPONIBLE");
					}
				}
				i++;
			}
			
			System.out.println("La Cantidad de prestamos es : " + librosPrestados);

			//Muestro los libros cargados.
			System.out.println("--------------------------------------------------------");
			System.out.printf("%-5s %-15s %-10s %-10s %-5s", "ISBN", "TITULO", "AUTOR", "PAGINAS", "DISPONIBLES");
			System.out.println();
			System.out.println("--------------------------------------------------------");
			i=0;
			while (i < 3){
				System.out.format("%-5s %-15s %-10s %-10s %-5s",libros[i].GetISBN(), libros[i].GetTitulo(), libros[i].GetAutor(),libros[i].GetPaginas(),libros[i].GetEjemplaresDisponibles());
				System.out.println();
				i++;
			}
			System.out.println("--------------------------------------------------------");

			Ecuacion ecu1;;
			ecu1 = new Ecuacion(1,-3,-4);
			i =0;
			if (ecu1.calcularRaices() [0] ==0) {
				System.out.println("NO TIENE RAICES");
			}
			if (ecu1.calcularRaices() [0] ==1) {
				System.out.println("TIENE SOLO 1 RAIZ: " + ecu1.calcularRaices() [1] );
			}
			if (ecu1.calcularRaices() [0] ==2) {
				System.out.println("TIENE 2 RAICES: " + ecu1.calcularRaices() [1] + " - " 
				+ ecu1.calcularRaices() [2] );
			}
			
			
		}
		
		
}
