
public class Libro {
	private int ISBN;
	private String Titulo;
	private String Autor;
	private int Paginas;
	private int CantEjemplares;
	private int EjemplaresPrestados;
	private int EjemplaresDisponibles;
	
	public Libro (int ISBN1, String Titulo1, String Autor1, int Paginas1, int CantEjemplares1) {
		ISBN = ISBN1;
		Titulo = Titulo1;
		Autor = Autor1;
		Paginas = Paginas1;
		CantEjemplares = CantEjemplares1;
		EjemplaresPrestados = CantEjemplares1;
		EjemplaresDisponibles = CantEjemplares1;
	}
	
	
	
	public int GetISBN() {
		return ISBN;
	}
	public String GetTitulo() {
		return Titulo;
	}
	public String GetAutor() {
		return Autor;
	}
	public int GetPaginas() {
		return Paginas;
	}
	public int GetCantPaginas() {
		return CantEjemplares;
	}
	public int GetEjemplaresPrestados() {
		return EjemplaresPrestados;
	}
	public int GetEjemplaresDisponibles() {
		return EjemplaresDisponibles;
	}
	
	public void SetISBN(int A) {
		ISBN = A;
	}
	public void SetTitulo(String A) {
		Titulo = A;
	}
	public void SetAutor(String A) {
		Autor = A;
	}
	public void SetPaginas(int A) {
		Paginas = A;
	}
	public void SetCantPaginas(int A) {
		CantEjemplares = A;
	}
	public void SetEjemplaresPrestados(int A) {
		EjemplaresPrestados = A;
	}
	public void SetEjemplaresDisponibles(int A) {
		EjemplaresDisponibles = A;
	}
		
		
		
}
